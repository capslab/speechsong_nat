var shuffleSequence = seq("intro",rshuffle(startsWith('ss'),startsWith('dt'),startsWith('tsmax'),startsWith('tsmin'),startsWith('shift')));

var counterOverride = 3;

var defaults = [
    "AcceptabilityJudgment", {
        as: ["1", "2", "3", "4", "5"],
        presentAsScale: true,
        timeout: 5000,
        instructions: "Use number keys or click boxes to answer.",
        leftComment: "(Unaltered)", rightComment: "(Synthesized)"
    },

    "Question", {
        hasCorrect: false
    },
    "Message", {
        hideProgressBar: true,
    },
    "Form", {
        hideProgressBar: true,
        continueOnReturn: false,
        saveReactionTime: false,
    }
];


var items = [

["intro", Message, {consentRequired: false,
                    html: ["div",
                            ["p", "Please put your headphones/earphones on, or make sure your speakers are working. You will hear a series of spoken phrases. After each phrase, you will have 5 seconds to indicate whether the phrase sounded more like synthesized speech (altered by a computer) or non-synthesized (unaltered) speech. After 5 seconds, if you have not responded, the program will automatically go on to the next phrase."]
                          ]}],


];




    for (i = 1; i < 49; i++) {
    var item1 = ["dt_" + i.toString() + ""];
item1.push("AudioMessage");
item1.push({consentRequired:false,transfer: 'audio-end', html: {include: i.toString() + "_detrended.html"}});
item1.push("AcceptabilityJudgment");
item1.push({s: "Listen to this phrase and rate how much it sounds like synthesized vs. non-synthesized speech, using the scale below."});
    items.push(item1);
    }

    for (i = 1; i < 49; i++) {
    var item1 = ["ss_" + i.toString() + ""];
item1.push("AudioMessage");
item1.push({consentRequired:false,transfer: 'audio-end', html: {include: i.toString() + ".html"}});
item1.push("AcceptabilityJudgment");
item1.push({s: "Listen to this phrase and rate how much it sounds like synthesized vs. non-synthesized speech, using the scale below."});
    items.push(item1);
    }

    for (i = 1; i < 49; i++) {
    var item1 = ["tsmax_" + i.toString() + ""];
item1.push("AudioMessage");
item1.push({consentRequired:false,transfer: 'audio-end', html: {include: i.toString() + "_timeshifted_max.html"}});
item1.push("AcceptabilityJudgment");
item1.push({s: "Listen to this phrase and rate how much it sounds like synthesized vs. non-synthesized speech, using the scale below."});
    items.push(item1);
    }

    for (i = 1; i < 49; i++) {
    var item1 = ["tsmin_" + i.toString() + ""];
item1.push("AudioMessage");
item1.push({consentRequired:false,transfer: 'audio-end', html: {include: i.toString() + "_timeshifted_min.html"}});
item1.push("AcceptabilityJudgment");
item1.push({s: "Listen to this phrase and rate how much it sounds like synthesized vs. non-synthesized speech, using the scale below."});
    items.push(item1);
    }
    
        for (i = 1; i < 49; i++) {
    var item1 = ["shift_" + i.toString() + ""];
item1.push("AudioMessage");
item1.push({consentRequired:false,transfer: 'audio-end', html: {include: i.toString() + "_shifted.html"}});
item1.push("AcceptabilityJudgment");
item1.push({s: "Listen to this phrase and rate how much it sounds like synthesized vs. non-synthesized speech, using the scale below."});
    items.push(item1);
    }