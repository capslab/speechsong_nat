#!/bin/bash
        for i in $( ls *.wav ); do
			echo "<!DOCTYPE html>" >> ${i%.*}.html
			echo "<html>" >> ${i%.*}.html
			echo "<body>" >> ${i%.*}.html
			echo "<audio class = 'audio-message' autoplay>" >> ${i%.*}.html
			echo "<source src='https://bitbucket.org/capslab/speechsong_nat/raw/master/Stimuli/"${i%.*}".wav' type='audio/wav'>" >> ${i%.*}.html
			echo "<source src='https://bitbucket.org/capslab/speechsong_nat/raw/master/Stimuli/"${i%.*}".mp3' type='audio/mpeg'>" >> ${i%.*}.html
			echo "<source src='https://bitbucket.org/capslab/speechsong_nat/raw/master/Stimuli/"${i%.*}".ogg' type='audio/ogg'>" >> ${i%.*}.html
			echo "</audio>" >> ${i%.*}.html
			echo "</body>" >> ${i%.*}.html
			echo "</html>" >> ${i%.*}.html
        done